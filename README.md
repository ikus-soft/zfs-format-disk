# zfs-format-disk

This script is responsible to properly create the partitions table required for a ZFS disk to be used on Proxmox VE server.

## Installation
As root:
```
curl http://git.patrikdufresne.com/pdsl/zfs-format-disk/raw/master/zfs-format-disk > /usr/local/sbin/zfs-format-disk
chmod +x /usr/local/sbin/zfs-format-disk
```

# Usage
To create the partition and instal grub on the disk:
```
zfs-format-disk /dev/sdf
```


# Changelog

## Aug 23, 2020

 * Adding support for EFI layout in addition of legacy layout
